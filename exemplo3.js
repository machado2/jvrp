var problemaExemplo3 = { 
    "capacidade_veiculo": 10,
    "deposito": {
        "latitude": -30.00846313410139,
        "longitude": -51.16607666015625
    },
    "destinos": [
        {
            "latitude": -30.021543509740013,
            "longitude": -51.10015869140625,
            "demanda": 1
        },
        {
            "latitude": -30.023921574501376,
            "longitude": -51.052093505859375,
            "demanda": 1
        },
        {
            "latitude": -30.01916538794234,
            "longitude": -50.9271240234375,
            "demanda": 1
        },
        {
            "latitude": -30.01084151252876,
            "longitude": -50.810394287109375,
            "demanda": 1
        },
        {
            "latitude": -30.004895459546216,
            "longitude": -50.712890625,
            "demanda": 1
        },
        {
            "latitude": -30.007273923504528,
            "longitude": -50.618133544921875,
            "demanda": 1
        },
        {
            "latitude": -30.01678720911106,
            "longitude": -50.511016845703125,
            "demanda": 1
        },
        {
            "latitude": -30.01678720911106,
            "longitude": -50.3887939453125,
            "demanda": 1
        },
        {
            "latitude": -30.041755241903317,
            "longitude": -50.288543701171875,
            "demanda": 1
        },
        {
            "latitude": -29.80132623415949,
            "longitude": -50.1800537109375,
            "demanda": 1
        },
        {
            "latitude": -29.532839863453397,
            "longitude": -50.225372314453125,
            "demanda": 1
        },
        {
            "latitude": -29.414479218562924,
            "longitude": -50.42724609375,
            "demanda": 1
        },
        {
            "latitude": -29.418067934281122,
            "longitude": -50.8447265625,
            "demanda": 1
        },
        {
            "latitude": -29.44677309638942,
            "longitude": -51.251220703125,
            "demanda": 1
        },
        {
            "latitude": -29.433617570990954,
            "longitude": -51.527252197265625,
            "demanda": 1
        },
        {
            "latitude": -29.450360671054405,
            "longitude": -51.70166015625,
            "demanda": 1
        },
        {
            "latitude": -29.468296641713216,
            "longitude": -52.086181640625,
            "demanda": 1
        },
        {
            "latitude": -29.52447547055937,
            "longitude": -52.296295166015625,
            "demanda": 1
        },
        {
            "latitude": -29.792984135470494,
            "longitude": -52.323760986328125,
            "demanda": 1
        },
        {
            "latitude": -30.110681880126062,
            "longitude": -52.2564697265625,
            "demanda": 1
        },
        {
            "latitude": -30.300575007090888,
            "longitude": -52.224884033203125,
            "demanda": 1
        },
        {
            "latitude": -30.409597432180085,
            "longitude": -52.0477294921875,
            "demanda": 1
        },
        {
            "latitude": -30.56226095049943,
            "longitude": -51.90765380859375,
            "demanda": 1
        },
        {
            "latitude": -30.733573157600027,
            "longitude": -51.9049072265625,
            "demanda": 1
        },
        {
            "latitude": -30.881012137733638,
            "longitude": -51.999664306640625,
            "demanda": 1
        },
        {
            "latitude": -31.089398067100554,
            "longitude": -51.90765380859375,
            "demanda": 1
        },
        {
            "latitude": -31.20810332132524,
            "longitude": -51.771697998046875,
            "demanda": 1
        },
        {
            "latitude": -31.28911345611945,
            "longitude": -51.107025146484375,
            "demanda": 1
        },
        {
            "latitude": -31.012925049646253,
            "longitude": -50.835113525390625,
            "demanda": 1
        },
        {
            "latitude": -30.679258712558674,
            "longitude": -50.565948486328125,
            "demanda": 1
        },
        {
            "latitude": -29.470687864832545,
            "longitude": -50.151214599609375,
            "demanda": 1
        },
        {
            "latitude": -29.40371231103247,
            "longitude": -50.06195068359375,
            "demanda": 1
        },
        {
            "latitude": -29.311548848196004,
            "longitude": -49.95758056640625,
            "demanda": 1
        },
        {
            "latitude": -29.11017598335443,
            "longitude": -49.67742919921875,
            "demanda": 1
        },
        {
            "latitude": -28.715882987724,
            "longitude": -49.483795166015625,
            "demanda": 1
        }
    ]
}
