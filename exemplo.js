var problemaExemplo = {
    "capacidade_veiculo": 5,
    "deposito": {
        "latitude": -29.86535844965241,
        "longitude": -51.15337371826172
    },
    "destinos": [
        {
            "latitude": -29.91804252607081,
            "longitude": -51.138267517089844,
            "demanda": 1
        },
        {
            "latitude": -29.903758071027728,
            "longitude": -51.213111877441406,
            "demanda": 1
        },
        {
            "latitude": -29.850173125689885,
            "longitude": -51.21997833251953,
            "demanda": 1
        },
        {
            "latitude": -29.83528331071022,
            "longitude": -51.257057189941406,
            "demanda": 1
        },
        {
            "latitude": -29.895424526240554,
            "longitude": -51.2841796875,
            "demanda": 1
        },
        {
            "latitude": -29.857617200624265,
            "longitude": -51.28314971923828,
            "demanda": 1
        },
        {
            "latitude": -29.882327547852505,
            "longitude": -51.31267547607422,
            "demanda": 1
        },
        {
            "latitude": -29.921315758451726,
            "longitude": -51.29447937011719,
            "demanda": 1
        },
        {
            "latitude": -29.954934549656134,
            "longitude": -51.26289367675781,
            "demanda": 1
        },
        {
            "latitude": -29.91209091878148,
            "longitude": -51.24778747558594,
            "demanda": 1
        },
        {
            "latitude": -29.840941701906264,
            "longitude": -51.2017822265625,
            "demanda": 1
        },
        {
            "latitude": -29.831411595220107,
            "longitude": -51.119384765625,
            "demanda": 1
        },
        {
            "latitude": -29.844515257507723,
            "longitude": -51.06548309326172,
            "demanda": 1
        },
        {
            "latitude": -29.883220578311054,
            "longitude": -51.041107177734375,
            "demanda": 1
        },
        {
            "latitude": -29.90643656239858,
            "longitude": -51.069602966308594,
            "demanda": 1
        },
        {
            "latitude": -29.870717424110236,
            "longitude": -51.09020233154297,
            "demanda": 1
        },
        {
            "latitude": -29.946902774133253,
            "longitude": -51.108055114746094,
            "demanda": 1
        },
        {
            "latitude": -29.980810301209154,
            "longitude": -51.031494140625,
            "demanda": 1
        },
        {
            "latitude": -29.93797781828953,
            "longitude": -50.93708038330078,
            "demanda": 1
        },
        {
            "latitude": -29.903162840943153,
            "longitude": -50.93708038330078,
            "demanda": 1
        },
        {
            "latitude": -29.850173125689885,
            "longitude": -51.00299835205078,
            "demanda": 1
        },
        {
            "latitude": -29.82158272057499,
            "longitude": -51.02703094482422,
            "demanda": 1
        },
        {
            "latitude": -29.87905303441006,
            "longitude": -51.00677490234375,
            "demanda": 1
        },
        {
            "latitude": -29.88560195376393,
            "longitude": -51.1138916015625,
            "demanda": 1
        },
        {
            "latitude": -29.863572060868286,
            "longitude": -51.12041473388672,
            "demanda": 1
        },
        {
            "latitude": -29.841239503090154,
            "longitude": -51.15406036376953,
            "demanda": 1
        },
        {
            "latitude": -29.94749774271586,
            "longitude": -51.14891052246094,
            "demanda": 1
        },
        {
            "latitude": -29.942737894394078,
            "longitude": -51.14032745361328,
            "demanda": 1
        },
        {
            "latitude": -29.951662423127154,
            "longitude": -51.141014099121094,
            "demanda": 1
        },
        {
            "latitude": -29.950770006293908,
            "longitude": -51.15337371826172,
            "demanda": 1
        },
        {
            "latitude": -29.937085278663098,
            "longitude": -51.15886688232422,
            "demanda": 1
        },
        {
            "latitude": -29.978728593469913,
            "longitude": -51.02531433105469,
            "demanda": 1
        },
        {
            "latitude": -29.98467621410636,
            "longitude": -51.03527069091797,
            "demanda": 1
        },
        {
            "latitude": -29.972185799395607,
            "longitude": -51.04248046875,
            "demanda": 1
        },
        {
            "latitude": -29.96326311304827,
            "longitude": -51.03218078613281,
            "demanda": 1
        },
        {
            "latitude": -29.93619273102959,
            "longitude": -50.94806671142578,
            "demanda": 1
        },
        {
            "latitude": -29.943927877830003,
            "longitude": -50.94154357910156,
            "demanda": 1
        },
        {
            "latitude": -29.940060379611825,
            "longitude": -51.32915496826172,
            "demanda": 1
        },
        {
            "latitude": -29.941547896721712,
            "longitude": -51.33567810058594,
            "demanda": 1
        },
        {
            "latitude": -29.94749774271586,
            "longitude": -51.33430480957031,
            "demanda": 1
        },
        {
            "latitude": -29.983784093718395,
            "longitude": -51.30889892578125,
            "demanda": 1
        },
        {
            "latitude": -29.980215532021585,
            "longitude": -51.31645202636719,
            "demanda": 1
        },
        {
            "latitude": -29.985865695489544,
            "longitude": -50.95390319824219,
            "demanda": 1
        },
        {
            "latitude": -29.982891965315776,
            "longitude": -50.96076965332031,
            "demanda": 1
        },
        {
            "latitude": -29.98824461550903,
            "longitude": -50.965576171875,
            "demanda": 1
        },
        {
            "latitude": -29.989136695821678,
            "longitude": -50.95390319824219,
            "demanda": 1
        },
        {
            "latitude": -29.98467621410636,
            "longitude": -50.940513610839844,
            "demanda": 1
        },
        {
            "latitude": -29.96802197880571,
            "longitude": -50.951499938964844,
            "demanda": 1
        }
    ]
}