class Local {
    constructor(lat, lng) {
        this.latitude = lat;
        this.longitude = lng;
    }
    distancia2(b) {
        var diflat = Math.abs(this.latitude - b.latitude);
        var diflng = Math.abs(this.longitude - b.longitude);
        return Math.sqrt(diflat * diflat + diflng * diflng);
    }

    distancia(b) {
        var lat1 = this.latitude * Math.PI / 180;
        var lon1 = this.longitude * Math.PI / 180;
        var lat2 = b.latitude * Math.PI / 180;
        var lon2 = b.longitude * Math.PI / 180;

        var a = Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1);
        var b = Math.sin(lat1) * Math.sin(lat2);
        return 6378137 * Math.acos(a + b);
    }


    maisProximo(lista) {
        var proximo = lista[0];
        var distProximo = this.distancia(proximo);
        for (var i = 0; i < lista.length; i++) {
            var dist = this.distancia(lista[i]);
            if (dist < distProximo) {
                proximo = lista[i];
                distProximo = dist;
            }
        }
        return proximo;
    }
}

class Destino extends Local {
    constructor(latitude, longitude, demanda) {
        super(latitude, longitude);
        this.demanda = demanda;
    }
}

function clonarObjeto(x) {
    return JSON.parse(JSON.stringify(x));
}

class PRVC {
    constructor() {
        this.capacidade_veiculo = 0;
        this.deposito = new Local();
        this.destinos = [];
    }
}