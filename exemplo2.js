var problemaExemplo2 = {
    "capacidade_veiculo": 8,
    "deposito": {
        "latitude": -29.794753165395836,
        "longitude": -51.15533709526062
    },
    "destinos": [
        {
            "latitude": -29.792006500253134,
            "longitude": -51.1553156375885,
            "demanda": 1
        },
        {
            "latitude": -29.792583771726697,
            "longitude": -51.15459680557251,
            "demanda": 1
        },
        {
            "latitude": -29.795088346489166,
            "longitude": -51.152440309524536,
            "demanda": 1
        },
        {
            "latitude": -29.794976619582798,
            "longitude": -51.15432858467102,
            "demanda": 1
        },
        {
            "latitude": -29.79664319965422,
            "longitude": -51.15584135055542,
            "demanda": 1
        },
        {
            "latitude": -29.79773251352622,
            "longitude": -51.15142107009888,
            "demanda": 1
        },
        {
            "latitude": -29.79642905963499,
            "longitude": -51.14807367324829,
            "demanda": 1
        },
        {
            "latitude": -29.79564697914973,
            "longitude": -51.148256063461304,
            "demanda": 1
        },
        {
            "latitude": -29.794976619582798,
            "longitude": -51.148996353149414,
            "demanda": 1
        },
        {
            "latitude": -29.79430625552492,
            "longitude": -51.14917874336243,
            "demanda": 1
        },
        {
            "latitude": -29.795069725346764,
            "longitude": -51.146249771118164,
            "demanda": 1
        },
        {
            "latitude": -29.794846271367717,
            "longitude": -51.14504814147949,
            "demanda": 1
        },
        {
            "latitude": -29.79447384696042,
            "longitude": -51.144211292266846,
            "demanda": 1
        },
        {
            "latitude": -29.79411073182877,
            "longitude": -51.15902781486511,
            "demanda": 1
        },
        {
            "latitude": -29.792704812097245,
            "longitude": -51.15235447883606,
            "demanda": 1
        },
        {
            "latitude": -29.793607954855826,
            "longitude": -51.15336298942566,
            "demanda": 1
        },
        {
            "latitude": -29.792946892399115,
            "longitude": -51.15558385848999,
            "demanda": 1
        },
        {
            "latitude": -29.793049310812084,
            "longitude": -51.1559271812439,
            "demanda": 1
        },
        {
            "latitude": -29.793151729120225,
            "longitude": -51.15892052650452,
            "demanda": 1
        },
        {
            "latitude": -29.795693531730663,
            "longitude": -51.1618173122406,
            "demanda": 1
        },
        {
            "latitude": -29.79212754132189,
            "longitude": -51.149983406066895,
            "demanda": 1
        }
    ]
}
