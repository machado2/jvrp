class AlgoritmoPRVC {
    constructor(prvc) {
        this.problema = prvc;
        this.melhorSolucao = this.ConstroiSolucaoAleatoria();
        this.melhorCusto = this.custoSolucao(this.melhorSolucao);
        this.finalizado = false;
    }

    // divide a lista de destinos em múltiplas viagens, de acordo com a capacidade do veículo
    divideViagens(destinos) {
        var viagens = [];
        var viagemAtual = [];
        var cargaVeiculo = 0;
        var posicaoVeiculo = this.problema.deposito;
        for (var i = 0; i < destinos.length; i++) {
            var proximo = destinos[i];
            if (viagemAtual.length > 0 && (cargaVeiculo + proximo.demanda) > this.problema.capacidade_veiculo) {
                viagens.push(viagemAtual);
                viagemAtual = [];
                cargaVeiculo = 0;
            }
            viagemAtual.push(proximo);
            cargaVeiculo += proximo.demanda;
        }
        if (viagemAtual.length > 0)
            viagens.push(viagemAtual);
        return viagens;
    }

    concatenaViagens(viagens) {
        var destinos = [];
        viagens.forEach(function(viagem) {
            destinos = destinos.concat(viagem);
        });
        return destinos;
    }

    listaDestinosAleatoria() {
        var destinos = [];
        var destinosRestantes = [];
        for (var i = 0; i < this.problema.destinos.length; i++)
            destinosRestantes.push(this.problema.destinos[i]);
        while (destinosRestantes.length > 0) {
            var indiceProximo = Math.floor(Math.random() * destinosRestantes.length);
            destinos.push(destinosRestantes[indiceProximo]);
            destinosRestantes.splice(indiceProximo, 1);
        }
        return destinos;
    }

    ConstroiSolucaoAleatoria() {
        var listaDestinos = this.listaDestinosAleatoria();
        var solucao = this.divideViagens(listaDestinos);
        return solucao;
    }

    troca2optvetor(vetor) {
        var i = Math.floor(Math.random() * vetor.length);
        var k = Math.floor(Math.random() * (vetor.length - 1));
        if (k >= i)
            k++;
        else
            [k, i] = [i, k];
        var a = vetor.slice(0, i);
        var b = vetor.slice(i, k + 1).reverse();
        var c = vetor.slice(k + 1);
        return a.concat(b).concat(c);
    }

    troca2opt(solucao) {
        var vetor = this.concatenaViagens(solucao);
        vetor = this.troca2optvetor(vetor);
        return this.divideViagens(vetor);
    }

    custoSolucao(viagens) {
        var total = 0;
        var problema = this.problema;
        viagens.forEach(function(viagem) {
            var pos = problema.deposito;
            viagem.forEach(function(destino) {
                total += pos.distancia(destino);
                pos = destino;
            });
            total += pos.distancia(problema.deposito);
        });
        return total;
    }
}

class AlgoritmoSempreMaisProximo extends AlgoritmoPRVC {
    constructor(prvc) {
        super(prvc);
    }

    calculaVrpMaisProximo() {
        var prvc = this.problema;
        var destinosRestantes = [];
        var posicaoVeiculo = prvc.deposito;
        var viagens = [];
        var viagemAtual = [];
        var cargaVeiculo = 0;
        for (var i = 0; i < prvc.destinos.length; i++)
            destinosRestantes.push(prvc.destinos[i]);
        while (destinosRestantes.length > 0) {
            var proximo = posicaoVeiculo.maisProximo(destinosRestantes);
            if (viagemAtual.length > 0 && (cargaVeiculo + proximo.demanda) > prvc.capacidade_veiculo) {
                viagens.push(viagemAtual);
                viagemAtual = [];
                cargaVeiculo = 0;
            }
            viagemAtual.push(proximo);
            cargaVeiculo += proximo.demanda;
            destinosRestantes.splice(destinosRestantes.indexOf(proximo), 1);
            posicaoVeiculo = proximo;
        }
        if (viagemAtual.length > 0)
            viagens.push(viagemAtual);
        return viagens;
    }

    executa() {
        if (this.finalizado)
            return false;
        this.melhorSolucao = this.calculaVrpMaisProximo();
        this.melhorCusto = this.custoSolucao(this.melhorSolucao);
        this.finalizado = true;
        return true;
    }
}



class Algoritmo2opt extends AlgoritmoPRVC {
    constructor(prvc) {
        super(prvc);
        this.iteracoes = 0;
    }

    executa() {
        var nova = this.troca2opt(this.melhorSolucao);
        var novoCusto = this.custoSolucao(nova);
        this.iteracoes++;
        if (novoCusto < this.melhorCusto) {
            this.melhorSolucao = nova;
            this.melhorCusto = novoCusto;
            return true;
        }
        return false;
    }
}

class AlgoritmoRecozimentoSimulado extends AlgoritmoPRVC {
    constructor(prvc) {
        super(prvc);
        this.iteracoes = 0;
        this.solucaoAtual = this.melhorSolucao;
        this.custoAtual = this.melhorCusto;
        this.energia = 0.5;
    }

    executa() {
        var nova = this.troca2opt(this.solucaoAtual);
        var novoCusto = this.custoSolucao(nova);
        this.iteracoes++;
        this.energia *= 0.9999;
        if (Math.random() < this.energia || novoCusto < this.custoAtual) {
            this.solucaoAtual = nova;
            this.custoAtual = novoCusto;
        }
        if (novoCusto < this.melhorCusto) {
            console.log('energia = ' + this.energia)
            this.melhorSolucao = nova;
            this.melhorCusto = novoCusto;
            return true;
        }
        return false;
    }
}

class AlgoritmoGenetico extends AlgoritmoPRVC {
    constructor(prvc) {
        super(prvc);
        this.iteracoes = 0;
        this.param_populacao = 1000;
        this.param_taxa_mutacao = 0.1;
        this.inicializa();
    }

    insereIndividuo(destinos) {
        var custo = this.custoSolucao(this.divideViagens(destinos));
        var fitness;
        if (custo > 0)
            fitness = 1 / custo;
        else
            fitness = 0;
        this.populacao.push({ destinos: destinos, fitness: fitness, custo: custo });
    }

    inicializa() {
        this.populacao = [];
        for (var i = 0; i < this.param_populacao; i++)
            this.insereIndividuo(this.listaDestinosAleatoria());
    }

    somaFitness() {
        var acum = 0;
        this.populacao.forEach(function(individuo) { acum += individuo.fitness; });
        return acum;
    }

    seleciona(totalFitness, lista) {
        var posFitness = Math.random() * totalFitness;
        var acum = 0;
        for (var i = 0; i < lista.length; i++) {
            acum += lista[i].fitness;
            if (acum >= posFitness)
                return lista[i];
        }
    }

    crossover(a, b) {
        var filho = [];
        var posicao = this.problema.deposito;

        var encontraProximo = function(pai) {
            var pos = pai.indexOf(posicao);
            for (var i = pos + 1; i < pai.length; i++) {
                var item = pai[i];
                if (filho.indexOf(item) === -1)
                    return item;
            }
            for (var i = 0; i < pai.length; i++) {
                var item = pai[i];
                if (filho.indexOf(item) === -1)
                    return item;
            }
            throw "crossover: posição não encontrada no pai";
        };

        while (filho.length < a.length) {
            var pa = encontraProximo(a);
            var pb = encontraProximo(b);
            if (posicao.distancia(pa) < posicao.distancia(pb))
                posicao = pa;
            else
                posicao = pb;
            filho.push(posicao);
        }
        return filho;
    }

    executa() {
        var totalFitness = this.somaFitness();
        var velhos = this.populacao;
        this.populacao = [];
        while (this.populacao.length < this.param_populacao) {
            var a = this.seleciona(totalFitness, velhos);
            var b = this.seleciona(totalFitness, velhos);
            var filho = this.crossover(a.destinos, b.destinos);
            if (Math.random() > this.param_taxa_mutacao) {
                filho = this.troca2optvetor(filho);
            }
            this.insereIndividuo(filho);
        }
        this.iteracoes++;
        this.populacao.sort(function(a, b) { return b.fitness - a.fitness; });
        if (this.populacao[0].custo < this.melhorCusto) {
            this.melhorSolucao = this.divideViagens(this.populacao[0].destinos);
            this.melhorCusto = this.populacao[0].custo;
            return true;
        }
        return false;
    }



}
